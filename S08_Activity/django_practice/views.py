from django.shortcuts import render
from django.http import HttpResponse
from .models import GroceryItem
# Create your views here.
def view(request):
    return HttpResponse("Hello from the view.py file!")

def index(request):
	grocery_items = GroceryItem.objects.all()
	context = {'grocery_list': grocery_items}
	return render(request, "django_practice/index.html", context)
